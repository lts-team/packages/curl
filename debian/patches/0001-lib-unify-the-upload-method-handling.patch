From 3037757e5c2ccf1ef34b0d7508cce90d2d6bcd49 Mon Sep 17 00:00:00 2001
From: Daniel Stenberg <daniel@haxx.se>
Date: Tue, 25 Apr 2023 08:28:01 +0200
Subject: lib: unify the upload/method handling

By making sure we set state.upload based on the set.method value and not
independently as set.upload, we reduce confusion and mixup risks, both
internally and externally.

Closes #11017
---
 lib/curl_rtmp.c  | 4 ++--
 lib/file.c       | 4 ++--
 lib/ftp.c        | 8 ++++----
 lib/http.c       | 4 ++--
 lib/imap.c       | 6 +++---
 lib/rtsp.c       | 4 ++--
 lib/setopt.c     | 6 ++----
 lib/smb.c        | 4 ++--
 lib/smtp.c       | 4 ++--
 lib/ssh-libssh.c | 6 +++---
 lib/ssh.c        | 6 +++---
 lib/tftp.c       | 8 ++++----
 lib/transfer.c   | 3 ++-
 lib/urldata.h    | 2 +-
 14 files changed, 34 insertions(+), 35 deletions(-)

diff --git a/lib/curl_rtmp.c b/lib/curl_rtmp.c
index f09f2f332..a8c0f68fd 100644
--- a/lib/curl_rtmp.c
+++ b/lib/curl_rtmp.c
@@ -213,7 +213,7 @@ static CURLcode rtmp_connect(struct connectdata *conn, bool *done)
   /* We have to know if it's a write before we send the
    * connect request packet
    */
-  if(conn->data->set.upload)
+  if(conn->data->state.upload)
     r->Link.protocol |= RTMP_FEATURE_WRITE;
 
   /* For plain streams, use the buffer toggle trick to keep data flowing */
@@ -244,7 +244,7 @@ static CURLcode rtmp_do(struct connectdata *conn, bool *done)
   if(!RTMP_ConnectStream(r, 0))
     return CURLE_FAILED_INIT;
 
-  if(conn->data->set.upload) {
+  if(conn->data->state.upload) {
     Curl_pgrsSetUploadSize(conn->data, conn->data->state.infilesize);
     Curl_setup_transfer(conn, -1, -1, FALSE, NULL, FIRSTSOCKET, NULL);
   }
diff --git a/lib/file.c b/lib/file.c
index 8bba3b916..c7084826c 100644
--- a/lib/file.c
+++ b/lib/file.c
@@ -196,7 +196,7 @@ static CURLcode file_connect(struct connectdata *conn, bool *done)
   file->freepath = real_path; /* free this when done */
 
   file->fd = fd;
-  if(!data->set.upload && (fd == -1)) {
+  if(!data->state.upload && (fd == -1)) {
     failf(data, "Couldn't open file %s", data->state.up.path);
     file_done(conn, CURLE_FILE_COULDNT_READ_FILE, FALSE);
     return CURLE_FILE_COULDNT_READ_FILE;
@@ -388,7 +388,7 @@ static CURLcode file_do(struct connectdata *conn, bool *done)
 
   Curl_pgrsStartNow(data);
 
-  if(data->set.upload)
+  if(data->state.upload)
     return file_upload(conn);
 
   file = conn->data->req.protop;
diff --git a/lib/ftp.c b/lib/ftp.c
index 5e4f156ad..ca96b262c 100644
--- a/lib/ftp.c
+++ b/lib/ftp.c
@@ -1384,7 +1384,7 @@ static CURLcode ftp_state_prepare_transfer(struct connectdata *conn)
                 data->set.str[STRING_CUSTOMREQUEST]:
                 (data->set.ftp_list_only?"NLST":"LIST"));
       }
-      else if(data->set.upload) {
+      else if(data->state.upload) {
         PPSENDF(&conn->proto.ftpc.pp, "PRET STOR %s", conn->proto.ftpc.file);
       }
       else {
@@ -3312,7 +3312,7 @@ static CURLcode ftp_done(struct connectdata *conn, CURLcode status,
     /* the response code from the transfer showed an error already so no
        use checking further */
     ;
-  else if(data->set.upload) {
+  else if(data->state.upload) {
     if((-1 != data->state.infilesize) &&
        (data->state.infilesize != *ftp->bytecountp) &&
        !data->set.crlf &&
@@ -3580,7 +3580,7 @@ static CURLcode ftp_do_more(struct connectdata *conn, int *completep)
                            connected back to us */
       }
     }
-    else if(data->set.upload) {
+    else if(data->state.upload) {
       result = ftp_nb_type(conn, data->set.prefer_ascii, FTP_STOR_TYPE);
       if(result)
         return result;
@@ -4269,7 +4269,7 @@ CURLcode ftp_parse_url_path(struct connectdata *conn)
     ftpc->file = NULL; /* instead of point to a zero byte, we make it a NULL
                           pointer */
 
-  if(data->set.upload && !ftpc->file && (ftp->transfer == FTPTRANSFER_BODY)) {
+  if(data->state.upload && !ftpc->file && (ftp->transfer == FTPTRANSFER_BODY)) {
     /* We need a file name when uploading. Return error! */
     failf(data, "Uploading to a URL without a file name!");
     return CURLE_URL_MALFORMAT;
diff --git a/lib/http.c b/lib/http.c
index ab755f7cb..bc6754fbf 100644
--- a/lib/http.c
+++ b/lib/http.c
@@ -2013,7 +2013,7 @@ CURLcode Curl_http(struct connectdata *conn, bool *done)
   http->writebytecount = http->readbytecount = 0;
 
   if((conn->handler->protocol&(PROTO_FAMILY_HTTP|CURLPROTO_FTP)) &&
-     data->set.upload) {
+     data->state.upload) {
     httpreq = HTTPREQ_PUT;
   }
 
@@ -2191,7 +2191,7 @@ CURLcode Curl_http(struct connectdata *conn, bool *done)
     if((conn->handler->protocol & PROTO_FAMILY_HTTP) &&
        (((httpreq == HTTPREQ_POST_MIME || httpreq == HTTPREQ_POST_FORM) &&
        http->postsize < 0) ||
-       (data->set.upload && data->state.infilesize == -1))) {
+       (data->state.upload && data->state.infilesize == -1))) {
       if(conn->bits.authneg)
         /* don't enable chunked during auth neg */
         ;
diff --git a/lib/imap.c b/lib/imap.c
index 23eb56dbd..2ac851180 100644
--- a/lib/imap.c
+++ b/lib/imap.c
@@ -1478,11 +1478,11 @@ static CURLcode imap_done(struct connectdata *conn, CURLcode status,
     result = status;         /* use the already set error code */
   }
   else if(!data->set.connect_only && !imap->custom &&
-          (imap->uid || imap->mindex || data->set.upload ||
+          (imap->uid || imap->mindex || data->state.upload ||
           data->set.mimepost.kind != MIMEKIND_NONE)) {
     /* Handle responses after FETCH or APPEND transfer has finished */
 
-    if(!data->set.upload && data->set.mimepost.kind == MIMEKIND_NONE)
+    if(!data->state.upload && data->set.mimepost.kind == MIMEKIND_NONE)
       state(conn, IMAP_FETCH_FINAL);
     else {
       /* End the APPEND command first by sending an empty line */
@@ -1553,7 +1553,7 @@ static CURLcode imap_perform(struct connectdata *conn, bool *connected,
     selected = TRUE;
 
   /* Start the first command in the DO phase */
-  if(conn->data->set.upload || data->set.mimepost.kind != MIMEKIND_NONE)
+  if(conn->data->state.upload || data->set.mimepost.kind != MIMEKIND_NONE)
     /* APPEND can be executed directly */
     result = imap_perform_append(conn);
   else if(imap->custom && (selected || !imap->mailbox))
diff --git a/lib/rtsp.c b/lib/rtsp.c
index 01dfce640..25a24e618 100644
--- a/lib/rtsp.c
+++ b/lib/rtsp.c
@@ -525,7 +525,7 @@ static CURLcode rtsp_do(struct connectdata *conn, bool *done)
      rtspreq == RTSPREQ_SET_PARAMETER ||
      rtspreq == RTSPREQ_GET_PARAMETER) {
 
-    if(data->set.upload) {
+    if(data->state.upload) {
       putsize = data->state.infilesize;
       data->set.httpreq = HTTPREQ_PUT;
 
@@ -544,7 +544,7 @@ static CURLcode rtsp_do(struct connectdata *conn, bool *done)
         result =
           Curl_add_bufferf(&req_buffer,
                            "Content-Length: %" CURL_FORMAT_CURL_OFF_T"\r\n",
-                           (data->set.upload ? putsize : postsize));
+                           (data->state.upload ? putsize : postsize));
         if(result)
           return result;
       }
diff --git a/lib/setopt.c b/lib/setopt.c
index 4e2ed05b3..fe4cf7dc1 100644
--- a/lib/setopt.c
+++ b/lib/setopt.c
@@ -255,8 +255,8 @@ CURLcode Curl_vsetopt(struct Curl_easy *data, CURLoption option,
      * We want to sent data to the remote host. If this is HTTP, that equals
      * using the PUT request.
      */
-    data->set.upload = (0 != va_arg(param, long)) ? TRUE : FALSE;
-    if(data->set.upload) {
+    arg = va_arg(param, long);
+    if(arg) {
       /* If this is HTTP, PUT is what's needed to "upload" */
       data->set.httpreq = HTTPREQ_PUT;
       data->set.opt_no_body = FALSE; /* this is implied */
@@ -522,7 +522,6 @@ CURLcode Curl_vsetopt(struct Curl_easy *data, CURLoption option,
     }
     else
       data->set.httpreq = HTTPREQ_GET;
-    data->set.upload = FALSE;
     break;
 
   case CURLOPT_COPYPOSTFIELDS:
@@ -832,7 +831,6 @@ CURLcode Curl_vsetopt(struct Curl_easy *data, CURLoption option,
      */
     if(va_arg(param, long)) {
       data->set.httpreq = HTTPREQ_GET;
-      data->set.upload = FALSE; /* switch off upload */
       data->set.opt_no_body = FALSE; /* this is implied */
     }
     break;
diff --git a/lib/smb.c b/lib/smb.c
index 9241d0951..5dfae899b 100644
--- a/lib/smb.c
+++ b/lib/smb.c
@@ -517,7 +517,7 @@ static CURLcode smb_send_open(struct connectdata *conn)
   byte_count = strlen(req->path);
   msg.name_length = smb_swap16((unsigned short)byte_count);
   msg.share_access = smb_swap32(SMB_FILE_SHARE_ALL);
-  if(conn->data->set.upload) {
+  if(conn->data->state.upload) {
     msg.access = smb_swap32(SMB_GENERIC_READ | SMB_GENERIC_WRITE);
     msg.create_disposition = smb_swap32(SMB_FILE_OVERWRITE_IF);
   }
@@ -789,7 +789,7 @@ static CURLcode smb_request_state(struct connectdata *conn, bool *done)
     smb_m = (const struct smb_nt_create_response*) msg;
     req->fid = smb_swap16(smb_m->fid);
     conn->data->req.offset = 0;
-    if(conn->data->set.upload) {
+    if(conn->data->state.upload) {
       conn->data->req.size = conn->data->state.infilesize;
       Curl_pgrsSetUploadSize(conn->data, conn->data->req.size);
       next_state = SMB_UPLOAD;
diff --git a/lib/smtp.c b/lib/smtp.c
index 19eb8734d..57bdafde5 100644
--- a/lib/smtp.c
+++ b/lib/smtp.c
@@ -1212,7 +1212,7 @@ static CURLcode smtp_done(struct connectdata *conn, CURLcode status,
     result = status;         /* use the already set error code */
   }
   else if(!data->set.connect_only && data->set.mail_rcpt &&
-          (data->set.upload || data->set.mimepost.kind)) {
+          (data->state.upload || data->set.mimepost.kind)) {
     /* Calculate the EOB taking into account any terminating CRLF from the
        previous line of the email or the CRLF of the DATA command when there
        is "no mail data". RFC-5321, sect. 4.1.1.4.
@@ -1304,7 +1304,7 @@ static CURLcode smtp_perform(struct connectdata *conn, bool *connected,
   smtp->eob = 2;
 
   /* Start the first command in the DO phase */
-  if((data->set.upload || data->set.mimepost.kind) && data->set.mail_rcpt)
+  if((data->state.upload || data->set.mimepost.kind) && data->set.mail_rcpt)
     /* MAIL transfer */
     result = smtp_perform_mail(conn);
   else
diff --git a/lib/ssh-libssh.c b/lib/ssh-libssh.c
index 333df03ef..256197390 100644
--- a/lib/ssh-libssh.c
+++ b/lib/ssh-libssh.c
@@ -1076,7 +1076,7 @@ static CURLcode myssh_statemach_act(struct connectdata *conn, bool *block)
     }
 
     case SSH_SFTP_TRANS_INIT:
-      if(data->set.upload)
+      if(data->state.upload)
         state(conn, SSH_SFTP_UPLOAD_INIT);
       else {
         if(protop->path[strlen(protop->path)-1] == '/')
@@ -1687,7 +1687,7 @@ static CURLcode myssh_statemach_act(struct connectdata *conn, bool *block)
       /* Functions from the SCP subsystem cannot handle/return SSH_AGAIN */
       ssh_set_blocking(sshc->ssh_session, 1);
 
-      if(data->set.upload) {
+      if(data->state.upload) {
         if(data->state.infilesize < 0) {
           failf(data, "SCP requires a known file size for upload");
           sshc->actualcode = CURLE_UPLOAD_FAILED;
@@ -1790,7 +1790,7 @@ static CURLcode myssh_statemach_act(struct connectdata *conn, bool *block)
         break;
       }
     case SSH_SCP_DONE:
-      if(data->set.upload)
+      if(data->state.upload)
         state(conn, SSH_SCP_SEND_EOF);
       else
         state(conn, SSH_SCP_CHANNEL_FREE);
diff --git a/lib/ssh.c b/lib/ssh.c
index 8c68adcc1..d493cdc37 100644
--- a/lib/ssh.c
+++ b/lib/ssh.c
@@ -1645,7 +1645,7 @@ static CURLcode ssh_statemach_act(struct connectdata *conn, bool *block)
     }
 
     case SSH_SFTP_TRANS_INIT:
-      if(data->set.upload)
+      if(data->state.upload)
         state(conn, SSH_SFTP_UPLOAD_INIT);
       else {
         if(sftp_scp->path[strlen(sftp_scp->path)-1] == '/')
@@ -2348,7 +2348,7 @@ static CURLcode ssh_statemach_act(struct connectdata *conn, bool *block)
         break;
       }
 
-      if(data->set.upload) {
+      if(data->state.upload) {
         if(data->state.infilesize < 0) {
           failf(data, "SCP requires a known file size for upload");
           sshc->actualcode = CURLE_UPLOAD_FAILED;
@@ -2487,7 +2487,7 @@ static CURLcode ssh_statemach_act(struct connectdata *conn, bool *block)
     break;
 
     case SSH_SCP_DONE:
-      if(data->set.upload)
+      if(data->state.upload)
         state(conn, SSH_SCP_SEND_EOF);
       else
         state(conn, SSH_SCP_CHANNEL_FREE);
diff --git a/lib/tftp.c b/lib/tftp.c
index 45f19871d..f54846f4f 100644
--- a/lib/tftp.c
+++ b/lib/tftp.c
@@ -391,7 +391,7 @@ static CURLcode tftp_parse_option_ack(tftp_state_data_t *state,
 
       /* tsize should be ignored on upload: Who cares about the size of the
          remote file? */
-      if(!data->set.upload) {
+      if(!data->state.upload) {
         if(!tsize) {
           failf(data, "invalid tsize -:%s:- value in OACK packet", value);
           return CURLE_TFTP_ILLEGAL;
@@ -470,7 +470,7 @@ static CURLcode tftp_send_first(tftp_state_data_t *state, tftp_event_t event)
       return result;
     }
 
-    if(data->set.upload) {
+    if(data->state.upload) {
       /* If we are uploading, send an WRQ */
       setpacketevent(&state->spacket, TFTP_EVENT_WRQ);
       state->conn->data->req.upload_fromhere =
@@ -505,7 +505,7 @@ static CURLcode tftp_send_first(tftp_state_data_t *state, tftp_event_t event)
     if(!data->set.tftp_no_options) {
       char buf[64];
       /* add tsize option */
-      if(data->set.upload && (data->state.infilesize != -1))
+      if(data->state.upload && (data->state.infilesize != -1))
         msnprintf(buf, sizeof(buf), "%" CURL_FORMAT_CURL_OFF_T,
                   data->state.infilesize);
       else
@@ -546,7 +546,7 @@ static CURLcode tftp_send_first(tftp_state_data_t *state, tftp_event_t event)
     break;
 
   case TFTP_EVENT_OACK:
-    if(data->set.upload) {
+    if(data->state.upload) {
       result = tftp_connect_for_tx(state, event);
     }
     else {
diff --git a/lib/transfer.c b/lib/transfer.c
index 3b97e5f2c..6b1e4857d 100644
--- a/lib/transfer.c
+++ b/lib/transfer.c
@@ -1469,6 +1469,7 @@ void Curl_init_CONNECT(struct Curl_easy *data)
 {
   data->state.fread_func = data->set.fread_func_set;
   data->state.in = data->set.in_set;
+  data->state.upload = (data->set.httpreq == HTTPREQ_PUT);
 }
 
 /*
@@ -1877,7 +1878,7 @@ CURLcode Curl_retry_request(struct connectdata *conn,
 
   /* if we're talking upload, we can't do the checks below, unless the protocol
      is HTTP as when uploading over HTTP we will still get a response */
-  if(data->set.upload &&
+  if(data->state.upload &&
      !(conn->handler->protocol&(PROTO_FAMILY_HTTP|CURLPROTO_RTSP)))
     return CURLE_OK;
 
diff --git a/lib/urldata.h b/lib/urldata.h
index 382aa8854..06a47fb5c 100644
--- a/lib/urldata.h
+++ b/lib/urldata.h
@@ -1404,6 +1404,7 @@ struct UrlState {
 #endif
   trailers_state trailers_state; /* whether we are sending trailers
                                        and what stage are we at */
+  bool upload;                   /* upload request */
 };
 
 
@@ -1680,7 +1681,6 @@ struct UserDefined {
   bool http_set_referer; /* is a custom referer used */
   bool http_auto_referer; /* set "correct" referer when following location: */
   bool opt_no_body;      /* as set with CURLOPT_NOBODY */
-  bool upload;           /* upload request */
   enum CURL_NETRC_OPTION
        use_netrc;        /* defined in include/curl.h */
   bool verbose;          /* output verbosity */
-- 
2.30.2

